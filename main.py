#! bin/python
from pathlib import Path
from crud_app.crud import CrudOperator
from utils.file_util import FileOperator
from utils.starter_util import StartOperator
from utils.profile_util import ProfileOperator
from utils.config_cr import ConfigOperator


def run() -> None:
    """
    Запуск приложения при запуске файла
    :return: None
    """
    # choose your profile:
    user_name = input("Enter you profile: ")
    # init ConfigOperator
    conf = ConfigOperator()
    # creation configure and app_work_dir
    if not conf.config_path.exists():
        conf.create_config()
    # read the json_path
    path = Path(conf.find_json_from_config())

    # init profile-object:
    profile = ProfileOperator(path, user_name)

    # init start-object:
    starter = StartOperator(user_name)
    print(starter.prewiew)

    # init file object:
    file = FileOperator(profile.file_path, profile.user_name)
    # check the json:
    if not path.exists():
        file.create_zero_file()
    # init crud-object:
    crud = CrudOperator(file.file_path, file.user_name)

    # START:
    starter.make_magic(profile.profile_finder(file.take_that_file)[-1], crud)
    print(starter.goodbye)


if __name__ == "__main__":
    run()
