import re
from utils.pwd_gen_util import PwdWork
from utils.file_util import FileOperator


class CrudOperator(FileOperator):
    """
    Класс круд операций
    :param: user: ключ с именем пользователя / профиля
    :param: file_path: путь до json файла
    """
    def __init__(self, file_path: str, user_name: str):
        super().__init__(file_path, user_name)

    @property
    def new_raw(self) -> dict:
        """ Создание и запись новых кред пользователя в файл
        :return: словарь новой строки, заданной пользователем
        """
        pwd_object = PwdWork()
        if pwd_object.make_pwd_flag():
            password = pwd_object.auto_creation_of_pwd()
        else:
            password = input("Enter the password: ")
        uid = {i: input(f'Enter the {i}: ') for i in {"service": "", "login": ""}}
        uid.update(password=password)
        return uid

    def create_raw(self) -> None:
        """ Метод запись в json новой строки с кредами """
        raw_dict = self.new_raw
        # if not Path(self.file_path).exists():
        #     self.create_new_file(raw_dict)
        # else:
        data = self.take_that_file
        while not self.check_key_at_that_file(
            data[self.user_name], raw_dict["service"]):
            
            raw_dict.update(service=input(
                f"{raw_dict['service']} - already written to the file. Choose a different service name\n:")
                            )     
        data[self.user_name].append(raw_dict)
        self.update_the_file_dict(data)
             
    def read_raws(self) -> None:
        """ Метод чтения всех нужных кред, заданных пользователем """
        self.table_creds_at_print(dict(self.take_that_file)[self.user_name],
                                  True, "Choose your services",
                                  "Login", "Password")
        user_input_list = [i for i in \
            list(map(lambda x: x.strip(), 
                     re.split("\W", input("choose the services you want to read: ")))) if i not in ("", " ")]
        file_data = self.take_that_file[self.user_name]
        file_services_list = [i["service"] for i in file_data]
        target_list = []
        for elem in user_input_list:
            try:
                if elem in file_services_list:
                    for dict_ in file_data:
                        if dict_["service"] == elem:
                           target_list.append(dict_)
                else:
                    raise ValueError
            except ValueError:
                print(f"{elem} is undefined for {self.user_name}")
        self.table_creds_at_print(target_list, False, "Service", "Login", "Password")

    def update_raw(self) -> None:
        """ для внесения изменений в определнный ключь кред """
        data = self.take_that_file
        new_data = data
        self.table_creds_at_print(data[self.user_name], True, 
                                  "Choose your services",
                                  "Login",
                                  "Password")
        choosen_service = input("Choose your service to update: ")
        key_for_update = input("What update: [service, login, password]? ")
        try:
            if key_for_update == "password":
                pwd_object = PwdWork()
                update_value = pwd_object.auto_creation_of_pwd() \
                    if pwd_object.make_pwd_flag() else input(f"Enter you new {key_for_update}: ")
            elif key_for_update in ["service", "login"]:
                update_value = input(f"Enter your new value for {key_for_update}: ")
            else:
                raise ValueError
        except ValueError:
            print("""The value of such a key is invalid. Enter the correct value from the list:
['service', 'login', 'password']""")
            self.update_raw()
            
        for di in new_data[self.user_name]:
            if di["service"] == choosen_service:
                di.update({f"{key_for_update}": update_value})
        self.update_the_file_dict(new_data)

    def delete_raw(self):
        data = self.take_that_file
        self.table_creds_at_print(data[self.user_name], True, 
                                  "Choose your services",
                                  "Login",
                                  "Password")
        choosen_service = input("Choose your service to delete: ")
        for dict_ in data[self.user_name]:
            data[self.user_name].remove(dict_) if dict_["service"] == choosen_service else None
        print(f"{choosen_service} is deleted from file...")
        self.update_the_file_dict(data)
   