import json
from pathlib import Path
from prettytable import PrettyTable


class FileOperator:
    def __init__(self, file_path, user_name):
        self.file_path = file_path
        self.user_name = user_name

    @property
    def take_that_file(self) -> dict:
        """
        для возврата json-объекта из имеющегося файла
        :return: сам json на конкретный профиль из файла
        """
        with open(self.file_path, "r", encoding="utf-8") as file:
            return json.load(file)

    def update_the_file_dict(self, data_to_write: dict) -> None:
        """
        для записи нового словаря кред пользователя
        :param data_to_write: исходный словарь с данными
        :return: None
        """
        with open(self.file_path, "w+", encoding="utf-8") as file:
            json.dump(data_to_write, file, ensure_ascii=False, indent=3)

    def create_zero_file(self) -> None:
        """ для проверки наличия json-файла с кредами
        """
        # необходимо продумать исключение метода - create_new_file]
        if not Path(self.file_path).exists():
            print("This user is a first user in our application!")
            with open(self.file_path, "w", encoding="utf-8") as file:
                json.dump({f"{self.user_name}": []}, file, ensure_ascii=False, indent=3)

    @staticmethod
    def table_creds_at_print(dicts_: list,
                             flag: bool,
                             *args) -> None:
        """ Метод для формирвоания шаблона вывода в теримнали таблицы 

        :param dicts_: словарь из файла
        :param flag: флаг как маркер как отображать (только сервисы или все креды)
        :return: None
        """
        table = PrettyTable()
        table.field_names = list(args)
        for elem_d in dicts_:
            table.add_row([i for i in elem_d.values()])
        if flag:
            print(table.get_string(fields=[args[0]]))
        else:
            print(table)

    @staticmethod
    def check_key_at_that_file(data_from_file: list, value_from_user: str) -> bool:
        """
        метод проверки наличия в файле уже записанных ключей сервисов под креды (логин и пароль)
        :param data_from_file: сам словарь под проверку
        :param value_from_user: искомый ключ в словаре, который передаст пользователь при
        создании нового сервиса
        :return: булевый флаг
        """
        return False if value_from_user in [dict_["service"] for dict_ in data_from_file] else True
