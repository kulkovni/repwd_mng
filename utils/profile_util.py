import sys
from pathlib import Path

sys.path.insert(0, str(Path.cwd()))

from utils.file_util import FileOperator
from utils.starter_util import StartOperator


class ProfileOperator(FileOperator):
    """
    Для работы с именем профиля (основным ключом в файле,
    который хранит словари с кредами)
    """
    def __init__(self, file_path, user_name):
        super().__init__(file_path, user_name)
    
    def check_new_profile_input(self, inp: str) -> bool:
        """ Для проверки истинного выбора пользователя:
        хочет он создать нового пользователя, или нет
        (рекурсия)

        :param inp: значение, которое необходимо для установки флага
        :return: булевый флаг
        """
        try:
            if inp in ["y", "yes"]:
                return True
            elif inp in ["n", "now"]:
                return False
            else:
                raise ValueError
        except ValueError:
            self.check_new_profile_input(input("Repeat you enter: "))
                
    def profile_finder(self, data: dict) -> tuple:
        """
        Для поиска основного ключа внутри файла
        
        :param data : имеющийся в json словарь на момент вызова метода
        :return: кортеж из: 
            а) обновленный словарь, с учетом нового ключа (пользователя) +
            б) флаг создания профиля, который нужен в методе StartOperator.condition()
            для определения работы с имеющимся словарем или с новым
        """
        profile_flag = True
        if self.user_name not in data.keys():
            choosen = input("Such a user is not recorded in the file. do you want to create a new user? (y/n) :")
            if self.check_new_profile_input(choosen):
                data.update({self.user_name: []})
                self.update_the_file_dict(data)
            else:
                print(StartOperator(self.user_name).goodbye)
                sys.exit(1)
        return data, profile_flag
