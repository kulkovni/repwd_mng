import configparser

from pathlib import Path


class ConfigOperator:
    """ Для создания конфигурационного файла, который 
    может содержать любую необходимую информацию о
    приложении, пользователе, уровне доступа и тп
    """
    config = configparser.ConfigParser()
    config_path = Path(f"{Path.home()}/.py_pass_v2/config.ini")
    file_path = Path(f"{Path.home()}/.py_pass_v2/creds.json")
    
    def __init__(self) -> None:
        if not Path(self.config_path).parents[0].exists():
            Path(self.config_path).parents[0].mkdir(parents=True)
    
    def create_config(self) -> None:
        """ Непосредственно само создание конфига с путем хранения кред """
        self.config.add_section("Config")
        self.config.set("Config", "File_path", str(self.file_path))
        
        with open(self.config_path, "a") as file:
            self.config.write(file)
    
    def find_json_from_config(self) -> str:
        """ Чтение конфигурационного файла,
        для возврата строки пути, где хранятся креды

        :return: путь json с кредами
        """
        self.config.read(self.config_path)
        return self.config.get("Config", "File_path")
        
           
if __name__ == "__main__":
    conf = ConfigOperator()
    if not conf.config_path.exists():
        conf.create_config()
    else:
        print(conf.find_json_from_config())
