import sys
from pathlib import Path
from pyfiglet import Figlet

sys.path.insert(0, str(Path.cwd()))

from crud_app.crud import CrudOperator


class StartOperator:
    prewiew = Figlet(font="slant").renderText("Welcome to PyPass_v2")
    goodbye = Figlet(font="slant").renderText("GoodBye")
    
    def __init__(self, user) -> None:
        self.user = user

    @staticmethod
    def condition(profile_flag: bool) -> str:
        """
        для передачи типа операции в работу
        :return: непосредсвтенно сама операция, которая планируется в использовании
        """
        if profile_flag:
            possible_options = ['c', 'r', 'u', 'd', 'q']
        else:
            possible_options = ['c', 'q']
        user_condition = input(f"Enter `what you do: \
{[i for i in possible_options[:-1]]} - CRUD-operations, or {possible_options[-1]} - for exit: ").strip()
        return user_condition
        
    def make_magic(self, check_profile: bool,
                   crud_oper: CrudOperator) -> None:
        """ для запуска всего приложения
        :param check_profile:
        :param crud_oper: экземпляр класса crud операций
        """
        condition = self.condition(check_profile)
        try:
            if condition == "c":
                crud_oper.create_raw()
            elif condition == "r":
                crud_oper.read_raws()
            elif condition == "u":
                crud_oper.update_raw()
            elif condition == "d":
                crud_oper.delete_raw()
            elif condition == "q":
                pass
            else:
                raise ValueError
        except ValueError:
            print("Operation is undefined")
            self.make_magic(check_profile, crud_oper)


if __name__ == "__main__":
    rrr = StartOperator("New_testost_nik")
    path = Path(f"{Path.home()}/.py_pass_ver_two/creds.json")
    
       