import secrets
import string


class PwdWork:
    SIMBOLS = string.ascii_letters + string.digits

    @staticmethod
    def make_pwd_flag() -> bool:
        """
        Метод для установки флага, по которуму будет принято решение о генерации пароля
        :return: True || False:
        """
        return False if input("Generate password (y/n)? ") != "y" else True

    def pwd_gen(self, length: int) -> str:
        """ Простой генератор паролей
        :param length: длинна пароля
        :param abcd: паттерн из которого забираем символы
        :return: строка с паролем
        """
        return str(''.join(secrets.choice(self.SIMBOLS) for i in range(length)))
    
    def auto_creation_of_pwd(self) -> str:
        """ Метод автогенерации пароля
        :return : строчка пароля
        """

        while True:
            try:
                length = int(input("Enter the desired password length: "))+1
            except ValueError:
                print("Specify the length as a integer!")
                continue
            else:
                password = self.pwd_gen(length)
                return password
